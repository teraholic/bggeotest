/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
		this.receivedEvent('deviceready');
		
		var bgGeo = window.BackgroundGeolocation;

		gBgGeo.on(
			// event
			"location",
			// success callback
			function( location, taskid ) {
				var coords = location.coords;
				var lat    = coords.latitude;
				var lng    = coords.longitude;

				this.geoLog( "- Location: " + JSON.stringify(location) );
				this.geoLocation( "LAT = " + lat + " : LNG = " + lng );
			},
			// error callback
			function( errorCode ) {
				this.geoLog( "- Location: BackgroundGeoLocation error: " + errorCode );
			}
		);
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
	},
	
	geoLocation: function( location ) {
		var oldLocation = $("#geolocation_location").text();
	
		console.log( location );
		$("#geolocation_location").text( oldLocation + "\r\n" + location );
	},

	geoLog: function( text, level ) {
		var oldLog = $("#geolocation_log").text();
	
		if ( level ) {
			level = level.toUpperCase();

			switch ( level ) {
				case "WARN":
					console.warn( text );
					break;
				case "ERROR":
					console.error( text );
					break;
				default:
					console.log( text );
					break;
			}
		} else {
			console.log( text );
		}

		$("#geolocation_log").text( oldLog + "\r\n" + text );
	}
};

app.initialize();