/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var gBgGeo = null;
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
		this.receivedEvent('deviceready');
		
		gBgGeo = window.BackgroundGeolocation;

		// location event
		gBgGeo.on(
			// event
			"location",
			// success callback
			function( location, taskid ) {
				var coords = location.coords;
				var lat    = coords.latitude;
				var lng    = coords.longitude;

				app.geoLog( "- Location: " + JSON.stringify(location) );

				gBgGeo.getOdometer( function( distance ) {
					console.log( "Distance travelled: ", distance );
					$("#geolocation_odometer").val( distance );

					getGeolocation( lat, lng );
				} );
			},
			// error callback
			function( errorCode ) {
				app.geoLog( "- Location: BackgroundGeoLocation error: " + errorCode, "ERROR" );
			}
		);

		// motionchange event
		gBgGeo.on(
			// event
			"motionchange",
			// success callback
			function( isMoving, taskid ) {
				app.geoLog( "- onMotionChange: " + isMoving );
			},
			// error callback
			function( errorCode ) {
				app.geoLog( "- onMotionChange: BackgroundGeoLocation error: " + errorCode );
			}
		);

		gBgGeo.ready(
			// params
			{},
			// success callback
			function( state ) {
				// This callback is executed when the plugin is ready to use.
				app.geoLog( "BackgroundGeolocation ready: " + state );
				if ( !state.enabled ) {
					gBgGeo.start();
				}
			},
			// error callback
			function( error ) {
				app.geoLog( "BackgroundGeolocation failed: " + error, "ERROR" );
			}
		);
	},

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
	},
	
	geoLocation: function( location ) {
		var tgtObj = $( "#geolocation_location" );
		var oldLocation = tgtObj.text();
	
		if ( oldLocation != "" ) {
			oldLocation += "\r\n";
		}

		console.log( location );

		tgtObj.text( oldLocation + app.getNowTimestamp() + " - " + location );
		tgtObj.scrollTop( tgtObj.height() );
	},

	geoLog: function( text, level ) {
		var tgtObj = $( "#geolocation_log" );
		var oldLog = tgtObj.text();

		if ( oldLog != "" ) {
			oldLog += "\r\n";
		}

		if ( level ) {
			level = level.toUpperCase();

			switch ( level ) {
				case "WARN":
					console.warn( text );
					break;
				case "ERROR":
					console.error( text );
					break;
				default:
					console.log( text );
					break;
			}
		} else {
			console.log( text );
		}

		tgtObj.text( oldLog + app.getNowTimestamp() + " - " + text );
		tgtObj.scrollTop( tgtObj.height() );
	},

	getNowTimestamp: function() {
		var nowDate = new Date();

		return nowDate.getFullYear() + "-" + nowDate.getMonth() + 1 + "-" + nowDate.getDate() 
			+ " " + nowDate.getHours() + ":" + nowDate.getMinutes() + ":" + nowDate.getSeconds() + "." + nowDate.getMilliseconds();
	}
};

app.initialize();

$( document ).ready( function() {
	$( "#update_setting_params" ).on( "click", function( event ){
		var params = {};

		$( ":text", "#geolocation_params_area" ).each( function() {
			var obj = $(this);
			var key = obj.prop( "name" );
			var val = obj.val();

			console.log( key + ": " + val );

			params[key] = val;
		} );

		gBgGeo.setConfig(
			// params
			params,
			// succsee callback
			function() {
				app.geoLog( "- setConfig: success." )
				alert( "Update success." );
			},
			// error callback
			function( errorCode ) {
				app.geoLog( "- setConfig: error: " + errorCode );
				alert( "Update failed." );
			}
		);
	} );

	$( "#get_status" ).on( "click", function( event ){
		gBgGeo.getState( function( state ) {
			$("#geolocation_status_log").text( app.getNowTimestamp() + "\r\n" + JSON.stringify( state ) );
			$("#geolocation_status_log").scrollTop( 0 );
		} );
	} );

	$( "#reset_odometer" ).on( "click", function( event ){
		gBgGeo.resetOdometer(
			function() {
				$("#geolocation_odometer").val( 0 );
				app.geoLog( "- resetOdometer: success." )
				alert( "OdoMeter reset success." );
			},
			function( errorCode ) {
				app.geoLog( "- resetOdometer: error: " + errorCode );
				alert( "OdoMeter reset failed." );
			}
		);
	} );
} );

function getGeolocation( latitude, longitude ) {
    try {
        nativegeocoder.reverseGeocode(
			// success callback
			function( result ) {
				var postalCd = result[0].postalCode;//郵便番号
				var area = result[0].administrativeArea;//行政区域
				var subArea = result[0].subAdministrativeArea;//町名?(データが取れないため詳しくはわからない)
				var geolocality = result[0].locality;//市区町村
				var geoSublocality = result[0].subLocality;//市区町村
				var tFare = result[0].thoroughfare;//～町目
				var subtFare = result[0].subThoroughfare;//番地(数値のみ)
	
				var locationdata = latitude + "," + longitude;
				var address = "";
	
				if(JSON.stringify(result[0].postalCode) != undefined){
					address = "〒" + postalCd + " ";
				}
	
				if(JSON.stringify(result[0].administrativeArea) != undefined){
					address =　address + area;
				}
	
				if(JSON.stringify(result[0].locality) != undefined){
					address = address + geolocality;
				}
	
				if(JSON.stringify(result[0].subLocality) != undefined){
					address = address + geoSublocality;
				}
	
				if(JSON.stringify(result[0].subAdministrativeArea) != undefined){
					address = address + subArea;
				}
				
				if(JSON.stringify(result[0].thoroughfare) != undefined){
					address = address + tFare + ", ";
				}
			
				if(JSON.stringify(result[0].subThoroughfare) != undefined){
					address = address + subtFare + "番地";
				}
	
				if(address == ""){
					address = locationdata;
				}

				app.geoLocation( "LAT = " + latitude + " : LNG = " + longitude + "\r\n(" + address + ")" );
			},
			// error callback
			function( err ) {
				console.error( JSON.stringify( err ) );

				app.geoLocation( "LAT = " + latitude + " : LNG = " + longitude );
			},
			// 緯度
			Number( latitude ),
			// 軽度
			Number( longitude ),
			// params
			{
				useLocale: true,
				defaultLocale: "ja_JP",
				maxResults: 1
			}
		);
    } catch( ex ) {
    	console.error( ex.message );
    }
}
