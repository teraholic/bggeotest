var geoLocation = {
	init: function() {
		gBgGeo = window.BackgroundGeolocation;

		gSchedule = [
			// "1 17:30-21:00",		// Sunday: 5:30pm-9:00pm
			// "2-6 9:00-17:00",		// Mon-Fri: 9:00am to 5:00pm
			// "2,4,6 20:00-00:00",	// Mon, Web, Fri: 8pm to midnight (next day)
			// "7 10:00-19:00"			// Sat: 10am-7pm
			"1-7 07:00-19:00"
		];

		// location event
		gBgGeo.on(
			// event
			"location",
			// success callback
			function( location, taskid ) {
				var coords = location.coords;
				var lat    = coords.latitude;
				var lng    = coords.longitude;

				app.geoLog( "- Location: " + JSON.stringify(location) );

				gBgGeo.getOdometer( function( distance ) {
					console.log( "Distance travelled: ", distance );
					$("#geolocation_odometer").val( distance );

					getGeolocation( lat, lng );
				} );
			},
			// error callback
			function( errorCode ) {
				app.geoLog( "- Location: BackgroundGeoLocation error: " + errorCode, "ERROR" );
			}
		);

		// motionchange event
		gBgGeo.on(
			// event
			"motionchange",
			// success callback
			function( isMoving, taskid ) {
				app.geoLog( "- onMotionChange: " + isMoving );
			},
			// error callback
			function( errorCode ) {
				app.geoLog( "- onMotionChange: BackgroundGeoLocation error: " + errorCode );
			}
		);
		
		gBgGeo.on(
			"schedule",
			function( state ) {
				if ( state.enabled ) {
					app.geoLog( "- scheduled start tracking" );
				} else {
					app.geoLog( "- scheduled stop tracking" );
				}
			}
		);

		gBgGeo.ready(
			// params
			{},		// resetで設定するので未指定
			// success callback
			function( state ) {
				// This callback is executed when the plugin is ready to use.
				app.geoLog( "BackgroundGeolocation ready: " + state );
				
				gBgGeo.reset(
					// params
					{
						url: "http://192.168.23.85:8080/repoapi/api/v2/set_location_info",
						params: {
							"key1": "value1",
							"key2": 12345,
							"key3": true
						},
						stopOnTerminate: false,
						startOnBoot: true,
						schedule: gSchedule
					},
					// success callback
					function(){
						updateSettingParams( true );
						resetOdometer( true );
		
						if ( !state.enabled ) {
							gBgGeo.start();
						}
					},
					// error callback
					function( error ) {
						app.geoLog( "reset failed: " + error, "ERROR" );
					}
				)
			},
			// error callback
			function( error ) {
				app.geoLog( "BackgroundGeolocation failed: " + error, "ERROR" );
			}
		);
	},

	geoLocation: function( location ) {
		var tgtObj = $( "#geolocation_location" );
		var oldLocation = tgtObj.text();
	
		if ( oldLocation != "" ) {
			oldLocation += "\r\n";
		}

		console.log( location );

		tgtObj.text( oldLocation + app.getNowTimestamp() + " - " + location );
		tgtObj.scrollTop( tgtObj[0].scrollHeight - tgtObj.height() );
	},
}