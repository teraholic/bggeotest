var geoLocation = {
	init: function( isReadyToStart, successFn, failureFn ) {
		gBgGeo = window.BackgroundGeolocation;

		gSchedule = [
			// "1 17:30-21:00",		// Sunday: 5:30pm-9:00pm
			// "2-6 9:00-17:00",		// Mon-Fri: 9:00am to 5:00pm
			// "2,4,6 20:00-00:00",	// Mon, Web, Fri: 8pm to midnight (next day)
			// "7 10:00-19:00"			// Sat: 10am-7pm
			"1-7 07:00-19:00"
		];

		// location event
		gBgGeo.on(
			// event
			"location",
			// success callback
			function( location, taskid ) {
				var coords = location.coords;
				var lat    = coords.latitude;
				var lng    = coords.longitude;

				app.geoLog( "- Location: " + JSON.stringify(location) );

				gBgGeo.getOdometer( function( distance ) {
					console.log( "Distance travelled: ", distance );
					$("#geolocation_odometer").val( distance );

					geoLocation.getLocationName( lat, lng );
				} );
			},
			// error callback
			function( errorCode ) {
				app.geoLog( "- Location: BackgroundGeoLocation error: " + errorCode, "ERROR" );
			}
		);

		// motionchange event
		gBgGeo.on(
			// event
			"motionchange",
			// success callback
			function( isMoving, taskid ) {
				app.geoLog( "- onMotionChange: " + isMoving );
			},
			// error callback
			function( errorCode ) {
				app.geoLog( "- onMotionChange: BackgroundGeoLocation error: " + errorCode );
			}
		);
		
		gBgGeo.on(
			"schedule",
			function( state ) {
				if ( state.enabled ) {
					app.geoLog( "- scheduled start tracking" );
				} else {
					app.geoLog( "- scheduled stop tracking" );
				}
			}
		);

		gBgGeo.ready(
			// params
			{},		// resetで設定するので未指定
			// success callback
			function( state ) {
				// This callback is executed when the plugin is ready to use.
				app.geoLog( "BackgroundGeolocation ready: " + state );
				
				gBgGeo.setConfig(
					// params
					{
						url: "http://192.168.23.85:8080/repoapi/api/v2/set_location_info",
						debug: true,
						logLevel :BackgroundGeolocation.LOG_LEVEL_VERBOSE,
						stopOnTerminate: false,
						startOnBoot: true
					},
					// success callback
					function(){
						if ( !state.enabled && isReadyToStart ) {
							gBgGeo.start();
						}

						if ( common.checkFunc( successFn ) ) {
							successFn( state );
						}
					},
					// error callback
					function( error ) {
						app.geoLog( "reset failed: " + error, "ERROR" );

						if ( common.checkFunc( failureFn ) ) {
							failureFn( error );
						}
					}
				)
			},
			// error callback
			function( error ) {
				app.geoLog( "BackgroundGeolocation failed: " + error, "ERROR" );
			}
		);
	},

	startTracking: function( params, successFn, failureFn ) {
		gBgGeo.start( 
			function() {
				console.debug( "Geo tracking start success." );

				if ( common.checkFunc( successFn ) ) {
					successFn();
				}
			},
			function() {
				console.debug( "Geo tracking start failed." );
				
				if ( common.checkFunc( failureFn ) ) {
					failureFn();
				}
			}
		);
	},

	stopTracking: function( successFn, failureFn ) {
		gBgGeo.stop( 
			function() {
				console.debug( "Geo tracking stop success." );

				if ( common.checkFunc( successFn ) ) {
					successFn();
				}
			},
			function() {
				console.debug( "Geo tracking stop failed." );

				if ( common.checkFunc( failureFn ) ) {
					failureFn();
				}
			}
		);
	},

	geoLocation: function( location ) {
		var tgtObj = $( "#geolocation_location" );
		var oldLocation = tgtObj.text();
	
		if ( oldLocation != "" ) {
			oldLocation += "\r\n";
		}

		console.log( location );

		tgtObj.text( oldLocation + app.getNowTimestamp() + " - " + location );
		tgtObj.scrollTop( tgtObj[0].scrollHeight - tgtObj.height() );
	},

	getLocationName: function( latitude, longitude ) {
		try {
			nativegeocoder.reverseGeocode(
				// success callback
				function( result ) {
					var postalCd = result[0].postalCode;//郵便番号
					var area = result[0].administrativeArea;//行政区域
					var subArea = result[0].subAdministrativeArea;//町名?(データが取れないため詳しくはわからない)
					var geolocality = result[0].locality;//市区町村
					var geoSublocality = result[0].subLocality;//市区町村
					var tFare = result[0].thoroughfare;//～町目
					var subtFare = result[0].subThoroughfare;//番地(数値のみ)
		
					var locationdata = latitude + "," + longitude;
					var address = "";
		
					if(JSON.stringify(result[0].postalCode) != undefined){
						address = "〒" + postalCd + " ";
					}
		
					if(JSON.stringify(result[0].administrativeArea) != undefined){
						address =　address + area;
					}
		
					if(JSON.stringify(result[0].locality) != undefined){
						address = address + geolocality;
					}
		
					if(JSON.stringify(result[0].subLocality) != undefined){
						address = address + geoSublocality;
					}
		
					if(JSON.stringify(result[0].subAdministrativeArea) != undefined){
						address = address + subArea;
					}
					
					if(JSON.stringify(result[0].thoroughfare) != undefined){
						address = address + tFare + ", ";
					}
				
					if(JSON.stringify(result[0].subThoroughfare) != undefined){
						address = address + subtFare + "番地";
					}
		
					if(address == ""){
						address = locationdata;
					}
	
					geoLocation.geoLocation( "LAT = " + latitude + " : LNG = " + longitude + "\r\n(" + address + ")" );
				},
				// error callback
				function( err ) {
					console.error( JSON.stringify( err ) );
	
					geoLocation.geoLocation( "LAT = " + latitude + " : LNG = " + longitude );
				},
				// 緯度
				Number( latitude ),
				// 軽度
				Number( longitude ),
				// params
				{
					useLocale: true,
					defaultLocale: "ja_JP",
					maxResults: 1
				}
			);
		} catch( ex ) {
			console.error( ex.message );
		}
	}
}