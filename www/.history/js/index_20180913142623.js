/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var gBgGeo = null;
var gDB = null;
var gPluginState = {};

var gSchedule = [];

var common = {
	checkFunc: function( execFunc ) {
		return ( execFunc && typeof( execFunc ) == "function" );
	},

	execFunc: function( execFunc, args ) {
		if ( execFunc && typeof( execFunc ) == "function" ) {
			if ( args && Array.isArray( args ) && args.length > 0 ) {
				for ( var i = 0; i < args.length; i++ ) {
					execCmd += ", args[" + i + "]";
				}

				execCmd = "execFunc(" + execCmd.substr(1) + ")";

				eval( execCmd );
			} else {
				execFunc();
			}
		}
	}
}

var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
		this.receivedEvent('deviceready');
		
		geoLocation.init( false, function() {
			getGeoLocationPluginStatus( function( state ) {
				geoLocation.startTracking();
			} );
		} );
	},

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
	},

	geoLog: function( text, level ) {
		var tgtObj = $( "#geolocation_log" );
		var oldLog = tgtObj.text();

		if ( oldLog != "" ) {
			oldLog += "\r\n";
		}

		if ( level ) {
			level = level.toUpperCase();

			switch ( level ) {
				case "WARN":
					console.warn( text );
					break;
				case "ERROR":
					console.error( text );
					break;
				default:
					console.log( text );
					break;
			}
		} else {
			console.log( text );
		}

		tgtObj.text( oldLog + app.getNowTimestamp() + " - " + text );
		tgtObj.scrollTop( tgtObj[0].scrollHeight - tgtObj.height() );
	},

	getNowTimestamp: function() {
		var nowDate = new Date();

		return nowDate.getFullYear() + "-" + ( "0" + (nowDate.getMonth() + 1) ).slice( -2 ) + "-" + ( "0" + nowDate.getDate() ).slice( -2 )
			+ " " + ( "0" + nowDate.getHours() ).slice( -2 ) + ":" + ( "0" + nowDate.getMinutes() ).slice( -2 ) + ":" + ( "0" + nowDate.getSeconds() ).slice( -2 ) + "." + ( "00" + nowDate.getMilliseconds() ).slice( -3 );
	}
};

app.initialize();

$( document ).ready( function() {
	gDB = window.openDatabase( "AppDB", "1.0", "LocalDatabase", 1000 );

	gDB.transaction(
		// done
		function(tx){
			//報告シート（作成中）管理者向け
			tx.executeSql(
				"CREATE TABLE IF NOT EXISTS location_log_table ("+
				"sheet_no TEXT PRIMARY KEY,"+	//報告シートNo
				"sheet_subject TEXT,"+	//報告タイトル
				"sheet_message TEXT,"+	//報告説明文（base64）
				"sheet_deadline_type TEXT,"+	//締切種別
				"sheet_deadline_date TEXT,"+	//締切日付
				"sheet_deadline_time TEXT,"+	//締切時刻
				"sheet_item_count TEXT,"+	//項目数
				"sheet_auto_approve TEXT,"+  //自動承認可否
				"sheet_item_info TEXT,"+	//報告項目（base64）
				"sheet_authorizer_usr_uuid TEXT,"+	//ユーザID・CSV文字列（承認者）
				"sheet_authorizer_notification TEXT,"+	//承認者への通知
				"sheet_reporter_usr_uuid TEXT,"+	//ユーザID・CSV文字列（報告者）
				"sheet_reporter_notification TEXT,"+	//報告者への通知
				"sheet_status TEXT,"+	//状態
				"sheet_ejabberd_id TEXT,"+	//ejabbredId
				"sheet_talk_id TEXT,"+	//トークID
				"sheet_sent_date TEXT,"+	//送信日時
				"sheet_update_date TEXT )", 	//更新日時
				[], 
				function(){
					console.log( "[executeSql] location_log_tableの作成と初期化に成功しました。" );
				}, 
				function( error ){
					console.error( "[executeSql] location_log_tableの作成に失敗しました。: " + error );
				}
			);
		},
		// error
		function(error){
			console.error( "[transaction] location_log_tableの作成に失敗しました。: " + error );
		},
		// complete
		function(){
			console.log( "[transaction] location_log_tableの作成と初期化に成功しました。" );
		}
	);

	$( "#update_setting_params" ).on( "click", function( event ){
		updateSettingParams();
	} );

	$( "#get_status" ).on( "click", function( event ){
		gBgGeo.getState( function( state ) {
			$( "#geolocation_status_log" ).text( app.getNowTimestamp() + "\r\n" + JSON.stringify( state ) );
			$( "#geolocation_status_log" ).scrollTop( 0 );
		} );
	} );

	$( "#reset_odometer" ).on( "click", function( event ){
		resetOdometer();
	} );

	$( "#get_logger_log" ).on( "click", function( event ){
		gBgGeo.getLog( function( log ) {
			var tgtObj = $( "#geolocation_logger_log" );
	
			console.log( log );
	
			tgtObj.text( app.getNowTimestamp() + "\r\n" + log );
			tgtObj.scrollTop( tgtObj[0].scrollHeight - tgtObj.height() );
		} );
	} );

	$( "#get_current_position" ).on( "click", function( event ){
		gBgGeo.getCurrentPosition(
			// success callback
			function( location ) {
				// This location is already persisted to plugin’s SQLite db.  
				// If you’ve configured #autoSync: true, the HTTP POST has already started.
				//console.log("- Current position received: ", location);
				var coords = location.coords;
				var lat    = coords.latitude;
				var lng    = coords.longitude;

				app.geoLog( "- Location: " + JSON.stringify(location) );

				gBgGeo.getOdometer( function( distance ) {
					console.log( "Distance travelled: ", distance );
					$("#geolocation_odometer").val( distance );

					geoLocation.getLocationName( lat, lng );
				} );
			},
			// error callback
			function( errorCode ) {
				alert( "An location error occurred: " + errorCode );
			},
			// options
			{
				timeout: 30,      // 30 second timeout to fetch location
				maximumAge: 5000, // Accept the last-known-location if not older than 5000 ms.
				desiredAccuracy: 10,  // Try to fetch a location with an accuracy of `10` meters.
				samples: 3,   // How many location samples to attempt.
				extras: {         // [Optional] Attach your own custom `metaData` to this location.  This metaData will be persisted to SQLite and POSTed to your server
					foo: "bar"  
				}
			}
		);
	} );

	$( "#toggle_changepace" ).on( "click", function( event ){
		var tgtObj = $( "#toggle_changepace" );
		var status = tgtObj.val().toUpperCase();
		var enabled = false;

		if ( status == "DISABLED" ) {
			enabled = true;
			tgtObj.val( "Enabled" );
		} else {
			enabled = false;
			tgtObj.val( "Disabled" );
		}

		gBgGeo.changePace(
			// enabled
			enabled,
			// success callback
			function() {
				app.geoLog( "- changePace: success." )
			},
			// error callback
			function( errorCode ) {
				app.geoLog( "- changePace: error: " + errorCode );
			}
		);
	} );

	$( "#toggle_scheduler" ).on( "click", function( event ){
		var tgtObj = $( "#toggle_scheduler" );
		var status = tgtObj.val().toUpperCase();

		if ( status == "DISABLED" ) {
			tgtObj.val( "Enabled" );

			gBgGeo.startSchedule( function() {
				app.geoLog( "- Scheduler started" );
			} );
		} else {
			tgtObj.val( "Disabled" );

			gBgGeo.stopSchedule( function() {
				app.geoLog( "- Scheduler stopped" );
			} );
		}
	} );

	$( "#toggle_tracking" ).on( "click", function( event ){
		var tgtObj = $( "#toggle_tracking" );
		var status = tgtObj.val().toUpperCase();

		if ( status == "STOP" ) {
			tgtObj.val( "START" );

			gBgGeo.start( function() {
				app.geoLog( "- start success. Geo tracking start." );
				alert( "Geo tracking start." );
			} );
		} else {
			tgtObj.val( "STOP" );

			gBgGeo.stop( function() {
				app.geoLog( "- stop success. Geo tracking stop." );
				alert( "Geo tracking stop." );
			} );
		}
	} );
} );

function getGeoLocationPluginStatus( afterFn ) {
	gBgGeo.getState( function( state ) {
		console.log( "- getGeoLocationPluginStatus - getState: <<< setting params >>>" );

		$.each( state, function( key, val ) {
			gPluginState[key] = val;

			if ( $("[name='" + key + "']", "#geolocation_params_area").length > 0 ) {
				$("[name='" + key + "']", "#geolocation_params_area").val( val );
				key = "[x]" + key;
			}

			console.log( key + ": " + val );
		} );

		// if ( common.checkFunc( afterFn ) ) {
		// 	afterFn( state );
		// }
		common.execFunc( afterFn, {state: state} );
	} );
}

function resetOdometer( isInit ) {
	gBgGeo.resetOdometer(
		function( location) {
			var coords = location.coords;
			var lat    = coords.latitude;
			var lng    = coords.longitude;

			$( "#geolocation_odometer" ).val( 0 );

			app.geoLog( "- Location: " + JSON.stringify( location ) );
			geoLocation.getLocationName( lat, lng );

			app.geoLog( "- resetOdometer: success." )
			if ( !isInit ) {
				alert( "OdoMeter reset success." );
			}
		},
		function( errorCode ) {
			app.geoLog( "- resetOdometer: error: " + errorCode );
			alert( "OdoMeter reset failed." );
		}
	);
}

function updateSettingParams( isInit ) {
	gBgGeo.getState( function( state ) {
		console.log( "- updateSettingParams - getState: <<< setting params >>>" );

		$.each( state, function( key, val ) {
			var newVal = val;
			var mark = "";

			if ( $("[name='" + key + "']", "#geolocation_params_area").length > 0 ) {
				newVal = $("[name='" + key + "']", "#geolocation_params_area").val();
				mark = "[x]"
			}

			console.log( mark + key + ": " + val + " -> " + newVal );

			state[key] = newVal;
		} );
	
		gBgGeo.setConfig(
			// params
			state,
			// succsee callback
			function() {
				app.geoLog( "- setConfig: success." )
				if ( !isInit ) {
					alert( "Update success." );
				}
			},
			// error callback
			function( errorCode ) {
				app.geoLog( "- setConfig: error: " + errorCode );
				alert( "Update failed." );
			}
		);
	} );
}
